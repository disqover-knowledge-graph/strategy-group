# Strategy Group to drive the Knowledge Graph platform DISQOVER

This project is confidential. Membership is by invitation and access is restricted to members.

## Quick links

- [List of all discussion threads](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/-/issues)
- [Start new discussion](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/-/issues/new)

## Account Access

To grant access, the system expects you to use the same login as you used for your initial registration, 
when following the link in your invitation email.
If you did that with a password, and then later sign via Google or Twitter, 
the system may consider these as different accounts, especially if the two email addresses are different.

To avoid this problem, you can connect all your accounts on your profile page
https://gitlab.com/profile/account
then you can use any of them in the future,
so forgetting your password won't be a problem.


## More User Help

### Reading discussion threads

To [see the list of all discussions](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/-/issues),
click the `Issues` tab from the left navigation bar (4th icon from the top). 
You can search, filter and sort the discussions there.

When you see any discussion of interest, you can enter it by clicking and then add your comment at the bottom.
You can also subscribe to the updates for any thread by selecting "Subscribe" in the Notification bar
at the very bottom on the right navigation panel (you may need to scroll down).

### Opening a new discussion

To [start new discussion thread](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/-/issues/new), 
click the `Issues` tab from the left navigation bar (4th icon from the top) and then the green button "New issue"
in the right top corner. Alternatively, from [the main project page](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential),
you can click the `+` in the navigation bar right below the description, and then select "New issue".
Simply add the title and description and click on the green button "Submit issue".

### Files
All files are shown on [the main project page](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/).
(At the time of writing there is only this README.md file.)
You can dowload the entire file collection with a single click 
on the download icon (last to the right of the "Find file" button).

To upload a new file, click on the pull-down menu `+` on [the main project page](https://gitlab.com/disqover-knowledge-graph/strategy-group-confidential/)
in the navigation bar above (3rd navigation row below the description) and select `Upload file`.

### Formatting
For advanced formatting, title/subtitle structure, itemization, images, videos, mathematics formulas and much more,
use the Markdown language https://docs.gitlab.com/ee/user/markdown.html
